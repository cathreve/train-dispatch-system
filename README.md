# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = Cathrine Evensen  
STUDENT ID = 563570

## Project description

This project is a train dispatch system that manages and registers train departures. 
It provides functionality for adding new departures, deleting existing departures, 
searching for departures by various choices, editing departures, updating the system clock, 
and sorting departures based on different choices.

## Project structure

The project is structured using packages to organize related classes. 
The main source files are stored in the `src` directory.
    The main files are stored in `main` directory.
    JUnit test classes are stored in the `test` directory. 

## Link to repository

https://gitlab.stud.idi.ntnu.no/cathreve/train-dispatch-system

## How to run the project
To run the project, you need to locate the main class, which is `TrainDispatchApp`. 
The class is initiated and started by the UI. The program takes input from the user through the Scanner, 
allowing them to interact with the train dispatch system. Input can be integers to navigate the different menus
or String and Integers to change or make new departures.

## How to run the tests

To run the tests, navigate to the `test` directory and execute the JUnit test classes. 
You can use an IDE like IntelliJ or run the tests using a build tool like Maven or Gradle.


## References

https://github.com/HolyBarrel/IDATT1001-PROG1-F2022