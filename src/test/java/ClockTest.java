import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;
public class ClockTest {
  private Clock clock;

  @BeforeEach
  public void initTest(){
    clock = new Clock();
    LocalTime initialTime = LocalTime.parse("16:00");
    clock.updateClock(initialTime);
  }

  @Nested
  @DisplayName("Positive tests for clock")
  public class positiveTestClock{
    @Test
    @DisplayName("getCurrentTime returns right value")
    void testGetCurrentTimeGetsRightTime() {
      LocalTime currentTime = clock.getCurrentTime();

      assertNotNull(currentTime);
      assertEquals(LocalTime.parse("16:00"), currentTime);
    }
    @Test
    @DisplayName("Update clock with valid time")
    void testUpdateClockValidTime() {
      LocalTime newTime = LocalTime.parse("18:00");
      clock.updateClock(newTime);
      assertEquals(newTime, clock.getCurrentTime());
    }
  }

  @Nested
  @DisplayName("Negative tests for clock")
  public class negativeTestClock{
    @Test
    @DisplayName("Update clock with invalid time, should throw illegal argument")
    void testUpdateClockInvalidTimeThrowsException() {
      LocalTime newTime = LocalTime.parse("12:00");
      assertThrows(IllegalArgumentException.class, () -> clock.updateClock(newTime));
    }

    @Test
    @DisplayName("getCurrentTime does not return a specific (wrong) time")
    void testGetCurrentTimeDoesNotReturnSpecificTime() {
      LocalTime currentTime = clock.getCurrentTime();

      assertNotNull(currentTime);
      assertNotEquals(LocalTime.parse("12:00"), currentTime);
    }
  }

}
