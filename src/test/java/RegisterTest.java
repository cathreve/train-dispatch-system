import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RegisterTest {
  private Register register;
  private TrainDeparture trainDeparture;

  @BeforeEach
  public void initTest(){
    register = new Register();

    //default for a few of the simple tests
    trainDeparture = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
  }

  @Nested
  @DisplayName("Positive test for register")
  public class positiveTestRegister{

    @Nested
    @DisplayName("Positive test for adding departures")
    public class positiveTestAddingDepartures {
      @Test
      @DisplayName("New train departure is added successfully")
      public void newTrainDepartureAddedDoesNotThrowException() {
        register.newTrainDeparture(trainDeparture);

        List<TrainDeparture> trainDepartures = register.getTrainDepartures();
        assertTrue(trainDepartures.contains(trainDeparture));
      }

      @Test
      @DisplayName("Multiple new train departures are added successfully")
      public void trainWithWithUniqeParametersAdded() {
        register.newTrainDeparture(new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1));
        register.newTrainDeparture(new TrainDeparture(2, "F5", LocalTime.parse("18:00"), "Oslo"));
        register.newTrainDeparture(new TrainDeparture(3, "F2", LocalTime.parse("17:00"), "Trondheim"));
        register.newTrainDeparture(new TrainDeparture(4, "F5", LocalTime.parse("13:00"), "Oslo", 4));

        List<TrainDeparture> trainDepartures = register.getTrainDepartures();
        assertEquals(4, trainDepartures.size());
      }

      @Test
      @DisplayName("Multiple new train departures are added successfully")
      public void trainWithSameDepartureTimeAdded() {
        register.newTrainDeparture(new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1));
        register.newTrainDeparture(new TrainDeparture(2, "F5", LocalTime.parse("18:00"), "Oslo"));
        register.newTrainDeparture(new TrainDeparture(3, "F5", LocalTime.parse("18:00"), "Trondheim"));
        register.newTrainDeparture(new TrainDeparture(4, "F5", LocalTime.parse("18:00"), "Oslo", 4));

        List<TrainDeparture> trainDepartures = register.getTrainDepartures();
        assertEquals(4, trainDepartures.size());
      }
    }

    @Nested
    @DisplayName("Positive tests for checking if track isnt occupied")
    public class positiveTestTrackIsNotOccupied{
      @Test
      @DisplayName("Track is not occupied when track is same but different departure times")
      public void trackIsNotOccupiedSameTrackDifferentTime() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Oslo", 1);

        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);
        assertDoesNotThrow(() -> register.trackIsNotOccupied(trainDeparture2));
      }

      @Test
      @DisplayName("Track is not occupied when track is different but same departure times")
      public void trackIsNotOccupiedDifferentTrackSametTime() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("12:00"), "Oslo", 2);

        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);

        assertDoesNotThrow(() -> register.trackIsNotOccupied(trainDeparture2));
      }

      @Test
      @DisplayName("Track is not occupied when track and departure times are different")
      public void trackIsNotOccupiedDifferentTrackAndDeparture() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Oslo", 2);

        register.newTrainDeparture(trainDeparture1);
        register.trackIsNotOccupied(trainDeparture2);
      }
    }

    @Nested
    @DisplayName("Positive test for sort")
    public class positiveTestSortRegister {
      @Test
      @DisplayName("Sort departures by departure time")
      public void sortDeparturesByDepartureTime() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Oslo", 1);


        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);

        List<TrainDeparture> sortedDepartures = register.sortDepartures("1");

        assertEquals(trainDeparture1, sortedDepartures.get(0));
        assertEquals(trainDeparture2, sortedDepartures.get(1));
      }

      @Test
      @DisplayName("Sort departures by departure time plus delay")
      public void sortDeparturesByDepartureTimePlusDelay() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Oslo", 3);

        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);
        register.updateDelay(trainDeparture1, 120);


        List<TrainDeparture> sortedDepartures = register.sortDepartures("2");

        assertEquals(trainDeparture1, sortedDepartures.get(1));
        assertEquals(trainDeparture2, sortedDepartures.get(0));
      }

      @Test
      @DisplayName("Sort departures by destination")
      public void sortDeparturesByDestination() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Valdres", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Bergen");
        var trainDeparture3 = new TrainDeparture(3, "F5", LocalTime.parse("13:00"), "Stavanger");

        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);
        register.newTrainDeparture(trainDeparture3);

        List<TrainDeparture> sortedDepartures = register.sortDepartures("3");

        assertEquals(trainDeparture1, sortedDepartures.get(2));
        assertEquals(trainDeparture2, sortedDepartures.get(0));
        assertEquals(trainDeparture3, sortedDepartures.get(1));
      }

      @Test
      @DisplayName("Sort departures by track")
      public void sortDeparturesByTrack() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Valdres", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Bergen", 4);
        var trainDeparture3 = new TrainDeparture(3, "F5", LocalTime.parse("13:00"), "Stavanger", 7);

        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);
        register.newTrainDeparture(trainDeparture3);

        List<TrainDeparture> sortedDepartures = register.sortDepartures("4");

        assertEquals(trainDeparture1, sortedDepartures.get(0));
        assertEquals(trainDeparture2, sortedDepartures.get(1));
        assertEquals(trainDeparture3, sortedDepartures.get(2));
      }

      @Test
      @DisplayName("Sort departures by train number")
      public void sortDeparturesByTrainNumber() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Valdres", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Bergen", 4);
        var trainDeparture3 = new TrainDeparture(3, "F5", LocalTime.parse("13:00"), "Stavanger", 7);

        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);
        register.newTrainDeparture(trainDeparture3);

        List<TrainDeparture> sortedDepartures = register.sortDepartures("5");

        assertEquals(trainDeparture1, sortedDepartures.get(0));
        assertEquals(trainDeparture2, sortedDepartures.get(1));
        assertEquals(trainDeparture3, sortedDepartures.get(2));
      }
    }

    @Nested
    @DisplayName("Positive test for search in register")
    public class positiveTestSearchInRegister {
      @Test
      @DisplayName("Search departure by train number")
      public void searchDepartureByTrainNumber() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Valdres", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Bergen", 4);

        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);

        int trainNumber = 2;

        TrainDeparture foundDeparture = register.searchDepartureTrainNum(trainNumber);

        assertNotNull(foundDeparture);
      }

      @Test
      @DisplayName("Search departure by destination, with correct upper and lower case format")
      public void searchDepartureByDestinationCorrectUpperLowerCaseFormatDoesNotThrowException() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Valdres", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Bergen", 4);

        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);

        String destination = "Valdres";

        List<TrainDeparture> foundDepartures = register.searchDepartureDestination(destination);

        assertNotNull(foundDepartures);
        assertEquals(1, foundDepartures.size());
      }

      @Test
      @DisplayName("Search departure by destination, with correct upper and lower case format")
      public void searchDepartureByDestinationRandomUpperLowerCaseFormatDoesNotThrowException() {
        var trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Valdres", 1);
        var trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Bergen", 4);

        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);

        String destination = "vAlDRes";

        List<TrainDeparture> foundDepartures = register.searchDepartureDestination(destination);

        assertNotNull(foundDepartures);
        assertEquals(1, foundDepartures.size());
      }
    }

    @Nested
    @DisplayName("Positive test for update delay")
    public class positiveTestUpdateDelay {
      @Test
      @DisplayName("Delay updates properly")
      public void testUpdateDelayUpdates() {
        int delay1 = 10;

        register.updateDelay(trainDeparture, delay1);

        assertEquals(Duration.ofMinutes(10), trainDeparture.getDelay());
      }

      @Test
      @DisplayName("See if delay updated multiple times adds up")
      public void testUpdateDelayMultipleUpdates() {
        int delay1 = 10;
        int delay2 = 5;

        register.updateDelay(trainDeparture, delay1);
        register.updateDelay(trainDeparture, delay2);

        assertEquals(Duration.ofMinutes(15), trainDeparture.getDelay());
      }

      @Test
      @DisplayName("Update Delay with Zero Minutes")
      public void testUpdateDelayZero() {
        int newDelayMinutes = 0;
        register.updateDelay(trainDeparture, newDelayMinutes);

        assertEquals(Duration.ZERO, trainDeparture.getDelay());
      }
    }

    @Nested
    @DisplayName("Positive test for update track")
    public class positiveTestUpdateTrack {
      @Test
      @DisplayName("Updates track properly")
      public void updatesTrackProperly(){
        TrainDeparture trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
        register.newTrainDeparture(trainDeparture1);

        int newTrack = 5;
        register.updateTrack(trainDeparture1, newTrack);

        assertEquals(5, trainDeparture1.getTrack());
      }

      @Test
      @DisplayName("Track that updates delay, with occupied track at new time gets assigned no track")
      public void testUpdateDelayTrackOccupied() {
        TrainDeparture trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
        TrainDeparture trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("12:30"), "Oslo", 1);
        register.newTrainDeparture(trainDeparture1);
        register.newTrainDeparture(trainDeparture2);

        int newDelayMinutes = 30;
        register.updateDelay(trainDeparture1, newDelayMinutes);

        assertEquals(-1, trainDeparture1.getTrack());
      }
    }

    @Nested
    @DisplayName("Positive test for delete departures after departure")
    public class positiveTestRemoveDepartedTrains {
      @Test
      @DisplayName("Train removed from list after it departed")
      public void removeDepartedTrains() {
        register.newTrainDeparture(new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1));
        register.newTrainDeparture(new TrainDeparture(2, "F5", LocalTime.parse("14:00"), "Oslo", 1));
        LocalTime timeOfDay = LocalTime.parse("13:00");
        register.removeDepartedTrains(timeOfDay);

        assertEquals(1, register.getTrainDepartures().size());
      }

      @Test
      @DisplayName("All trains should have departed")
      public void removeAllDepartedTrains() {
        register.newTrainDeparture(new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1));
        register.newTrainDeparture(new TrainDeparture(2, "F5", LocalTime.parse("16:00"), "Oslo", 1));
        LocalTime timeOfDay = LocalTime.parse("17:00");
        register.removeDepartedTrains(timeOfDay);

        assertEquals(0, register.getTrainDepartures().size());
      }
    }

    @Nested
    @DisplayName("Positive tests for removing departure by train number")
    public class positiveTestRemoveTrainByNumber{
      @Test
      @DisplayName("Train is successfully removed by number input")
      public void trainSuccessfullyRemovedByNumber(){
        register.newTrainDeparture(trainDeparture);
        register.removeTrain(register.searchDepartureTrainNum(1));

        assertEquals(0, register.getTrainDepartures().size());
      }
    }
  }

  @Nested
  @DisplayName("Negative test for register")
  public class negativeTestRegister {

    @Nested
    @DisplayName("Negative tests for adding a new train departure")
    public class negativeTestAddTrainDeparture {
      @Test
      @DisplayName("Duplicate train number throws exception")
      public void duplicateTrainNumberNotAdded() {
        TrainDeparture trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
        TrainDeparture trainDeparture2 = new TrainDeparture(1, "F5", LocalTime.parse("13:00"), "Oslo");

        register.newTrainDeparture(trainDeparture1);

        assertThrows(IllegalArgumentException.class,
                () -> register.newTrainDeparture(trainDeparture2));
      }

      @Test
      @DisplayName("Duplicate train number throws exception")
      public void duplicateTrainNumberThrowsException() {
        TrainDeparture trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
        TrainDeparture trainDeparture2 = new TrainDeparture(1, "F5", LocalTime.parse("13:00"), "Oslo");

        register.newTrainDeparture(trainDeparture1);

        assertThrows(IllegalArgumentException.class,
                () -> register.newTrainDeparture(trainDeparture2));
      }
    }

    @Nested
    @DisplayName("Negative tests for sorting departures")
    public class negativeTestsSortDepartures {
      @Test
      @DisplayName("Invalid choice input for sorter throws exception")
      public void invalidInputForSorterThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> {
          register.newTrainDeparture(new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1));
          register.sortDepartures("6");
        });
      }
      @Test
      @DisplayName("Negative choice input for sorter throws exception")
      public void negativeInputForSorterThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> {
          register.newTrainDeparture(new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1));
          register.sortDepartures("-2");
        });
      }
    }

    @Nested
    @DisplayName("Negative tests for search methods")
    public class negativeTestsSearchMethods {
      @Test
      @DisplayName("Search for non-existent train number is null")
      public void searchNonExistentTrainNumberIsNull() {
        register.newTrainDeparture(trainDeparture);
        assertNull(register.searchDepartureTrainNum(3));
      }
      @Test
      @DisplayName("Search for non-existent destination is null")
      public void searchNonExistentDestinationIsNull() {
        register.newTrainDeparture(trainDeparture);

        assertNull(register.searchDepartureDestination("Trondheim"));
      }
    }

    @Nested
    @DisplayName("Negative tests for update delay")
    public class negativeTestsUpdateDelay {
      @Test
      @DisplayName("Update delay to when track is already assigned, should throw exception")
      public void updateDelayToAlreadyAssignedTrackThrowsException() {
        try {
          TrainDeparture trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
          TrainDeparture trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("13:00"), "Bergen", 1);
          register.newTrainDeparture(trainDeparture2);

          int newDelayMinutes = 60;
          register.updateDelay(trainDeparture1, newDelayMinutes);
        } catch (IllegalArgumentException e) {
          assertEquals("Track was changed to -1 due to unavailability.", e.getMessage());
        }
      }
      @Test
      @DisplayName("Update delay")
      public void updateDelayNegativeTimeThrowsException() {
        TrainDeparture trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);

        int newDelayMinutes = -10;

        assertThrows(IllegalArgumentException.class, () -> register.updateDelay(trainDeparture1, newDelayMinutes));
      }
    }

    @Nested
    @DisplayName("Negative tests for update track")
    public class negativeTestUpdateTrack {
      @Test
      @DisplayName("Update Track - Invalid Track Assignment (Occupied)")
      public void testUpdateTrackInvalidOccupied() {
        try {
          TrainDeparture trainDeparture1 = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
          TrainDeparture trainDeparture2 = new TrainDeparture(2, "F5", LocalTime.parse("12:00"), "Oslo");
          register.newTrainDeparture(trainDeparture1);
          register.newTrainDeparture(trainDeparture2);

          int newTrack = 1;
          register.updateTrack(trainDeparture2, newTrack);
        } catch (IllegalArgumentException e) {
          assertEquals("Track was changed to -1 due to unavailability.", e.getMessage());
        }

      }

      @Test
      @DisplayName("Update track with invalid track throws exception")
      public void testUpdateTrackInvalidNegative() {
        int newTrack = -5;
        assertThrows(IllegalArgumentException.class, () -> register.updateTrack(trainDeparture, newTrack));
      }
    }

    @Nested
    @DisplayName("Negative tests for removing departed trains")
    public class negativeTestRemoveDepartedTrains {
      @Test
      @DisplayName("No trains have departed")
      public void removeNoTrains() {
        register.newTrainDeparture(new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1));
        register.newTrainDeparture(new TrainDeparture(2, "F5", LocalTime.parse("16:00"), "Oslo", 1));
        LocalTime timeOfDay = LocalTime.parse("11:00");
        register.removeDepartedTrains(timeOfDay);

        assertEquals(2, register.getTrainDepartures().size());
      }
      @Test
      @DisplayName("No trains removed when currentTime is exactly a departure time")
      public void noTrainsRemovedAtExactDepartureTime() {
        TrainDeparture trainDeparture = new TrainDeparture(1, "F4", LocalTime.parse("12:00"), "Bergen", 1);
        register.newTrainDeparture(trainDeparture);
        LocalTime timeOfDay = LocalTime.parse("12:00");
        register.removeDepartedTrains(timeOfDay);

        assertEquals(1, register.getTrainDepartures().size());
      }
    }

    @Nested
    @DisplayName("Negative tests for removing departure by train number")
    public class negativeTestRemoveTrainByNumber{
      @Test
      @DisplayName("No train removed when train number doesnt exist")
      public void trainNotRemovedByNumberWhenTrainDoesNotExist(){
        register.newTrainDeparture(trainDeparture);
        register.removeTrain(register.searchDepartureTrainNum(3));

        assertEquals(1, register.getTrainDepartures().size());
      }
    }
  }
}
