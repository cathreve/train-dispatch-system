import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

public class TrainDepartureTest {
    private TrainDeparture trainDeparture;

    private int TRAIN_NUM = 3;
    private String LINE = "F4";
    private LocalTime DEPARTURE_TIME = LocalTime.parse("12:00");
    private String DESTINATION = "Oslo";
    private final int TRACK = 2;
    private final int NO_TRACK = -1;

    @BeforeEach
    public void initTest(){
        trainDeparture = new TrainDeparture(TRAIN_NUM, LINE, DEPARTURE_TIME, DESTINATION, TRACK);
    }


    @Nested
    @DisplayName("Positive test for TrainDeparture")
    public class PositiveTrainDepartureTests {

        @Test
        @DisplayName("TrainDeparture constructor does not throw an exception on valid input, with track")
        public void trainDepartureConstructorDoesNotThrowExceptionWithTrack() {
            assertNotNull(trainDeparture);
            assertEquals(TRAIN_NUM, trainDeparture.getTrainNumber());
            assertEquals(LINE, trainDeparture.getLine());
            assertEquals(DEPARTURE_TIME, trainDeparture.getDepartureTime());
            assertEquals(DESTINATION, trainDeparture.getDestination());
            assertEquals(TRACK, trainDeparture.getTrack());
            assertEquals(Duration.ZERO, trainDeparture.getDelay());
        }

        @Test
        @DisplayName("TrainDeparture constructor does not throw an exception on valid input, without track")
        public void trainDepartureConstructorDoesNotThrowExceptionWithoutTrack() {
            trainDeparture = new TrainDeparture(TRAIN_NUM, LINE, DEPARTURE_TIME, DESTINATION);

            assertNotNull(trainDeparture);
            assertEquals(TRAIN_NUM, trainDeparture.getTrainNumber());
            assertEquals(LINE, trainDeparture.getLine());
            assertEquals(DEPARTURE_TIME, trainDeparture.getDepartureTime());
            assertEquals(DESTINATION, trainDeparture.getDestination());
            assertEquals(NO_TRACK, trainDeparture.getTrack());
            assertEquals(Duration.ZERO, trainDeparture.getDelay());
        }

        @Test
        @DisplayName("Set track does not throw an exception on valid input")
        public void setTrackDoesNotThrowException() {
            var trainDeparture = new TrainDeparture(TRAIN_NUM, LINE, DEPARTURE_TIME, DESTINATION, TRACK);
            trainDeparture.setTrack(5);

            assertEquals(5, trainDeparture.getTrack());
        }
        @Test
        @DisplayName("Set delay does not throw an exception on valid input")
        public void setDelayDoesNotThrowException() {
            var trainDeparture = new TrainDeparture(TRAIN_NUM, LINE, DEPARTURE_TIME, DESTINATION, TRACK);
            trainDeparture.setDelay(Duration.ofMinutes(6));

            assertEquals(Duration.ofMinutes(6), trainDeparture.getDelay());
        }
    }

    /**
     * Negative tests for the class TrainDepartures
     * Here I test for the constructor
     * Set track
     * Set delay
     *
     */
    @Nested
    @DisplayName("Negative test for TrainDeparture")
    public class NegativeTrainDepartureTests {
        @Test
        @DisplayName("TrainDeparture constructor does throw an exception on invalid train number")
        public void trainDepartureConstructorDoesThrowExceptionWithInvalidTrainNumber() {
            TRAIN_NUM = -3;

            assertThrows(IllegalArgumentException.class, () -> {
                new TrainDeparture(TRAIN_NUM, LINE, DEPARTURE_TIME, DESTINATION, TRACK);
            });
        }

        @Test
        @DisplayName("TrainDeparture constructor does throw an exception on empty line")
        public void trainDepartureConstructorDoesThrowExceptionWithInvalidLine() {
            LINE = "";

            assertThrows(IllegalArgumentException.class, () -> {
                new TrainDeparture(TRAIN_NUM, LINE, DEPARTURE_TIME, DESTINATION, TRACK);
            });
        }

        @Test
        @DisplayName("TrainDeparture constructor does throw an exception on departure time")
        public void trainDepartureConstructorDoesThrowExceptionWithInvalidDepartureTime() {
            DEPARTURE_TIME = null;

            assertThrows(IllegalArgumentException.class, () -> {
                new TrainDeparture(TRAIN_NUM, LINE, DEPARTURE_TIME, DESTINATION, TRACK);
            });
        }

        @Test
        @DisplayName("TrainDeparture constructor does throw an exception on empty destination")
        public void trainDepartureConstructorDoesThrowExceptionWithInvalidDestination() {
            DESTINATION = "";

            assertThrows(IllegalArgumentException.class, () -> {
                new TrainDeparture(TRAIN_NUM, LINE, DEPARTURE_TIME, DESTINATION, TRACK);
            });
        }

        @Test
        @DisplayName("Set track does throw an exception on invalid input")
        public void setTrackDoesThrowException() {
            assertThrows(IllegalArgumentException.class, () -> trainDeparture.setTrack(-5));
        }

        @Test
        @DisplayName("Set delay does throw an exception on invalid input")
        public void setDelayDoesThrowException() {
            assertThrows(IllegalArgumentException.class, () -> trainDeparture.setDelay(Duration.ofMinutes(-6)));
        }
    }
}