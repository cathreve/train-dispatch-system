/**
 * The {@code MenuPrinter} class provides static methods for printing menus in a train departure
 * management system.
 *
 * <p><b>Note:</b> This class is not meant to be instantiated. All methods are static, and users
 * should call them directly without creating an instance of the class.</p>
 *
 * @author 10047
 * @version 0.1
 */
public class MenuPrinter {
  /**
   * Private constructor to ensure that no other classes can create objects of the MenuPrinter
   * class.
   */
  private MenuPrinter() {}

  /**
   * Prints the main menu options for the train departure management system.
   */
  protected static void printMainMenu() {
    System.out.println(
            """
            ----------------------------------
            Main menu
            1. See all train departures
            2. Search for departures
            3. Edit, add or delete departures
            4. Update clock
            5. Sort departures
            0. Exit
            ----------------------------------
            """);
  }

  /**
   * Prints the search menu options for the train departure management system.
   */
  protected static void printSearchMenu() {
    System.out.println(
            """
            ----------------------------------
            Search with:
            1. Train number
            2. Destination
            0. Back to main menu
            ----------------------------------
            """);
  }

  /**
   * Prints the edit menu options for the train departure management system.
   */
  protected static void printEditMenu() {
    System.out.println(
            """
            ----------------------------------
            Edit:
            1. Add new departure
            2. Edit delay
            3. Edit track
            4. Remove departure
            0. Back to main menu
            ----------------------------------
            """);
  }

  protected static void printSortMenu() {
    System.out.println(
            """
            ----------------------------------
            Sort by:
            1. Departure time
            2. Departure time plus delay
            3. Destination
            4. Track
            5. Train number
            0. Back to main menu
            ----------------------------------
            """);
  }
}
