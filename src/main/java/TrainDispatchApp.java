/**
 * The {@code TrainDispatchApp} class is an application class.
 * Launches the user interface via a main method.
 */
public class TrainDispatchApp {

  /**
   * Main-method that runs the application.
   *
   * @param args are the arguments for the main-method.
   */
  public static void main(String[] args) {
    UserInterface userInterface = new UserInterface();

    userInterface.start();
  }
}
