import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The {@code Register} class represents the register for train departures.
 * It contains methods to edit or alter the list of train departures.
 *
 * @author 10047
 * @version 0.1
 */

public class Register {
  private final ArrayList<TrainDeparture> trainDepartures;

  public Register() {
    trainDepartures = new ArrayList<>();
  }

  /**
   * Returns list of train departures.
   *
   * @return ArrayList of train departures.
   */
  public ArrayList<TrainDeparture> getTrainDepartures() {
    return trainDepartures;
  }

  /**
   * Add new train-departure.
   *
   * @param trainDeparture The new train departure to be added.
   */
  public void newTrainDeparture(TrainDeparture trainDeparture) {
    if (trainIsNotDuplicate(trainDeparture)) {
      if (trackIsNotOccupied(trainDeparture)) {
        trainDepartures.add(trainDeparture);
      } else {
        throw new IllegalArgumentException(
                "Departure could not be added due to time and track conflict.");
      }
    } else {
      throw new IllegalArgumentException(
              "Departure could not be added due to duplicate train number.");
    }
  }

  /**
   * Check if a train with the same train number and departure time already exists.
   *
   * @param newDeparture The new train departure to be checked.
   * @return {@code true} if a duplicate doesn't exist, {@code false} otherwise.
   */
  public boolean trainIsNotDuplicate(TrainDeparture newDeparture) {
    return trainDepartures.stream()
            .noneMatch(existingDeparture
                    -> existingDeparture.getTrainNumber() == newDeparture.getTrainNumber());
  }

  /**
   * Check if a train with the same departure and track number already exists.
   *
   * @param newDeparture The new train departure to be checked.
   * @return {@code true} if track isn't taken, {@code false} otherwise.
   */
  public boolean trackIsNotOccupied(TrainDeparture newDeparture) {
    if (newDeparture.getTrack() == -1) {
      return true;
    }

    return trainDepartures.stream()
            .noneMatch(existingDeparture ->
               existingDeparture.getDepartureWithDelay()
              .equals(newDeparture.getDepartureWithDelay())
               && existingDeparture.getTrack() == newDeparture.getTrack());
  }


  /**
   * Sorts a list of TrainDeparture objects based on the specified criterion.
   *
   * @param choice A String representing the sorting criterion:
   *               "1" for sorting by departure time,
   *               "2" for sorting by departure time with delay,
   *               "3" for sorting by destination,
   *               "4" for sorting by track,
   *               "5" for sorting by train number.
   * @return A List of TrainDeparture objects sorted according to the specified criterion.
   * @throws IllegalArgumentException if the provided choice is not a valid sorting criterion.
   */
  public List<TrainDeparture> sortDepartures(String choice) {
    Comparator<TrainDeparture> comparator = switch (choice) {
      case "1" -> Comparator.comparing(TrainDeparture::getDepartureTime);
      case "2" -> Comparator.comparing(TrainDeparture::getDepartureWithDelay);
      case "3" -> Comparator.comparing(TrainDeparture::getDestination);
      case "4" -> Comparator.comparing(TrainDeparture::getTrack);
      case "5" -> Comparator.comparing(TrainDeparture::getTrainNumber);
      default -> throw new IllegalArgumentException("Invalid sorting criterion: " + choice);
    };

    return trainDepartures.stream()
            .sorted(comparator)
            .collect(Collectors.toList());
  }


  /**
   * Search for a train departure by train number.
   *
   * @param trainNumber The train number to be searched
   * @return the departure with train number, or null if nothing was found.
   */
  public TrainDeparture searchDepartureTrainNum(int trainNumber) {
    return trainDepartures.stream()
            .filter(departure -> departure.getTrainNumber() == trainNumber)
            .findFirst()
            .orElse(null);
  }

  /**
   * Makes a list of all train departures with same destination.
   *
   * @param destination The destination to be searched.
   * @return list of departures with chosen destination, list is empty if no departure is found.
   */
  public List<TrainDeparture> searchDepartureDestination(String destination) {
    List<TrainDeparture> matchingDepartures = trainDepartures.stream()
            .filter(departure -> departure.getDestination().equalsIgnoreCase(destination))
            .collect(Collectors.toList());

    return matchingDepartures.isEmpty() ? null : matchingDepartures;
  }



  /**
   * Updates delay of departure.
   * If delay is updated to a when another train already is assigned the same track,
   * the train with updated delay loses its assigned track and gets assigned no track (-1)
   *
   * @param trainDeparture To find departure to add delay to.
   * @param newDelayMinutes Added delay, in minutes.
   * @throws IllegalArgumentException if track is occupied.
   */
  public void updateDelay(TrainDeparture trainDeparture, int newDelayMinutes) {
    Duration newDelay = Duration.ofMinutes(newDelayMinutes);
    trainDeparture.setDelay(newDelay);

    try {
      if (!trackIsNotOccupied(trainDeparture)) {
        trainDeparture.setTrack(-1);
        throw new IllegalArgumentException("Track was changed to -1 due to unavailability.");
      }
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Updates the track for a given train departure based on the input track number.
   * A temporary train departure is created to check if the track is occupied at the specified time.
   *
   * @param trainDeparture The train departure for which to update the track.
   * @param inputTrack The new track number to be assigned.
   * @throws IllegalArgumentException if track is occupied.
   */
  public void updateTrack(TrainDeparture trainDeparture, int inputTrack) {
    TrainDeparture tempDeparture = new TrainDeparture(
            trainDeparture.getTrainNumber(),
            trainDeparture.getLine(),
            trainDeparture.getDepartureTime(),
            trainDeparture.getDestination(),
            inputTrack
    );
    tempDeparture.setDelay(trainDeparture.getDelay());

    try {
      if (trackIsNotOccupied(tempDeparture)) {
        trainDeparture.setTrack(inputTrack);
      } else {
        throw new IllegalArgumentException("Track occupied, and was therefore not assigned.");
      }
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Removes train departures that have already departed based on the specified current time.
   *
   * @param currentTime The current time of day, chosen by user.
   */
  public void removeDepartedTrains(LocalTime currentTime) {
    getTrainDepartures().removeIf(trainDeparture ->
            trainDeparture.getDepartureWithDelay().isBefore(currentTime));
  }

  /**
   * Removes train departures based on the train number.
   *
   * @param trainDeparture the departure that user wants to delete.
   */
  public void removeTrain(TrainDeparture trainDeparture) {
    getTrainDepartures().remove(trainDeparture);
  }
}
