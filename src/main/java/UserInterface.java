import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

/**
 * The {@code UserInterface} class represents the UI for a train departure management system.
 * It provides methods for interacting with the system,
 * such as adding, searching, and editing train departures.
 *
 * @author 10047
 * @version 0.1
 */
public class UserInterface {
  private Register register;
  private Clock clock;

  //Default for all menus
  private static final String EXIT_MENU = "0";

  //Static variables for the main menu
  private static final String PRINT_REGISTER = "1";
  private static final String SEARCH_DEPARTURE = "2";
  private static final String EDIT_DEPARTURE = "3";
  private static final String UPDATE_CLOCK = "4";
  private static final String SORT_REGISTER = "5";

  //Options for search menu
  private static final String SEARCH_TRAIN_NUM = "1";
  private static final String SEARCH_DESTINATION = "2";

  //Options for edit menu
  private static final String ADD_DEPARTURE = "1";
  private static final String EDIT_DELAY = "2";
  private static final String EDIT_TRACK = "3";
  private static final String DELETE_DEPARTURE = "4";

  //Options for sort menu
  private static final String SORT_DEPARTURE = "1";
  private static final String SORT_DEPARTURE_PLUS_DELAY = "2";
  private static final String SORT_DESTINATION = "3";
  private static final String SORT_TRACK = "4";
  private static final String SORT_TRAIN_NUM = "5";

  /**
   * Method to start application.
   */
  public void start() {
    init();
    preAddedDepartures();
    menuOperations();
  }

  /**
   * Adds of pre-made train departures to register, checks for any error.
   * Made with help from ChatGPT
   */
  public void preAddedDepartures() {
    List<TrainDeparture> trainDepartures = Arrays.asList(
            new TrainDeparture(43, "F6", LocalTime.parse("08:02"), "Trondheim", 1),
            new TrainDeparture(47, "F6", LocalTime.parse("16:25"), "Trondheim", 6),
            new TrainDeparture(61, "F4", LocalTime.parse("06:25"), "Bergen"),
            new TrainDeparture(67, "F4", LocalTime.parse("16:25"), "Bergen", 6),
            new TrainDeparture(605, "F4", LocalTime.parse("23:03"), "Bergen", 5),
            new TrainDeparture(475, "RE30", LocalTime.parse("10:20"), "Gjøvik"),
            new TrainDeparture(1121, "R21", LocalTime.parse("10:20"), "Moss"),
            new TrainDeparture(1121, "R21", LocalTime.parse("10:30"), "Moss")
    );

    for (TrainDeparture trainDeparture : trainDepartures) {
      try {
        register.newTrainDeparture(trainDeparture);
      } catch (IllegalArgumentException e) {
        System.out.println("One of items could not be added to the register for the "
                + "following reason: " + e.getMessage());
      }
    }
  }

  /**
   * Initializes the clock and register.
   */
  public void init() {
    this.clock = new Clock();
    register = new Register();
  }

  /**
   * Contains the different operations for the main menu.
   */
  public void menuOperations() {
    MenuPrinter.printMainMenu();
    while (true) {
      removeDepartedTrains();
      switch (Scan.scanMenu()) {
        case PRINT_REGISTER -> printSortedDepartures(SORT_DEPARTURE);
        case SEARCH_DEPARTURE -> searchDepartureMenu();
        case EDIT_DEPARTURE -> editDepartureMenu();
        case UPDATE_CLOCK -> updateClock();
        case SORT_REGISTER -> sortRegisterMenu();
        case EXIT_MENU -> System.exit(0);
        default -> System.out.println("You have to write a valid number");
      }
      MenuPrinter.printMainMenu();
    }
  }

  /**
   * Menu for searching for departures.
   */
  public void searchDepartureMenu() {
    boolean exit = false;
    while (!exit) {
      MenuPrinter.printSearchMenu();
      switch (Scan.scanMenu()) {
        case SEARCH_TRAIN_NUM -> searchDepartureTrainNum();
        case SEARCH_DESTINATION -> searchDepartureDestination();
        case EXIT_MENU -> exit = true;
        default -> System.out.println("Please write a valid number");
      }
    }
  }

  /**
   * Menu for editing or adding new departures.
   */
  public void editDepartureMenu() {
    boolean exit = false;
    while (!exit) {
      MenuPrinter.printEditMenu();
      switch (Scan.scanMenu()) {
        case ADD_DEPARTURE -> newTrainDeparture();
        case EDIT_DELAY -> updateDelay();
        case EDIT_TRACK -> updateTrack();
        case DELETE_DEPARTURE -> removeTrainByTrainNumber();
        case EXIT_MENU -> exit = true;
        default -> System.out.println("Please write a valid number");
      }
    }
  }

  /**
   * Menu for editing or adding new departures.
   */
  public void sortRegisterMenu() {
    boolean exit = false;
    while (!exit) {
      MenuPrinter.printSortMenu();
      switch (Scan.scanMenu()) {
        case SORT_DEPARTURE -> printSortedDepartures(SORT_DEPARTURE);
        case SORT_DEPARTURE_PLUS_DELAY -> printSortedDepartures(SORT_DEPARTURE_PLUS_DELAY);
        case SORT_DESTINATION -> printSortedDepartures(SORT_DESTINATION);
        case SORT_TRACK -> printSortedDepartures(SORT_TRACK);
        case SORT_TRAIN_NUM -> printSortedDepartures(SORT_TRAIN_NUM);
        case EXIT_MENU -> exit = true;
        default -> System.out.println("Please write a valid number");
      }
    }
  }

  /**
   * Prints a sorted list of TrainDeparture objects based on the specified choice.
   *
   * @param choice A String representing the sorting criterion:
   *               "1" for sorting by departure time,
   *               "2" for sorting by departure time with delay,
   *               "3" for sorting by destination,
   *               "4" for sorting by track,
   *               "5" for sorting by train number.
   */
  public void printSortedDepartures(String choice) {
    printTableHeader();
    List<TrainDeparture> sortedDepartures = register.sortDepartures(choice);

    sortedDepartures.forEach(System.out::println);
  }

  /**
   * User can dd new train-departure.
   */
  public void newTrainDeparture() {
    System.out.println("Add new train-departure.");

    System.out.println("Train number: ");
    int trainNumber = Scan.scanInteger("Train number");

    System.out.println("Line: ");
    String line = Scan.scanLine();

    System.out.println("Time of departure: ");
    LocalTime departureTime = Scan.scanDepartureTime();

    System.out.println("Destination: ");
    String destination = Scan.scanDestination();

    System.out.println("Track: (press enter to continue without choosing track)");
    int track = Scan.scanInteger("Track");

    TrainDeparture trainDeparture = new TrainDeparture(trainNumber, line, departureTime,
            destination, track);
    register.newTrainDeparture(trainDeparture);
  }

  /**
   * Removes trains that have already departed.
   */
  public void removeDepartedTrains() {
    register.removeDepartedTrains(clock.getCurrentTime());
  }

  /**
   * User can search for departure with train number.
   */
  public void searchDepartureTrainNum() {
    System.out.println("Enter train number to search:");
    int trainNumber = Scan.scanInteger("Train number");
    TrainDeparture result = register.searchDepartureTrainNum(trainNumber);

    if (result != null) {
      printTableHeader();
      System.out.println(result);
    } else {
      System.out.println("Train departure not found.");
    }
  }

  /**
   * User can search for departure with destination.
   */
  public void searchDepartureDestination() {
    System.out.println("Enter destination to search:");
    String destination = Scan.scanDestination();
    List<TrainDeparture> results = register.searchDepartureDestination(destination);

    if (!results.isEmpty()) {
      System.out.println("Matching train departures:");
      printTableHeader();
      for (TrainDeparture result : results) {
        System.out.println(result);
      }
    } else {
      System.out.println("No matching departures found.");
    }
  }

  /**
   * Adds delay to a train, chosen by train number.
   */
  public void updateDelay() {
    System.out.println("Enter train number for which you want to update the delay:");
    int trainNumber = Scan.scanInteger("Train number");

    TrainDeparture trainDeparture = register.searchDepartureTrainNum(trainNumber);

    if (trainDeparture != null) {
      System.out.println("Enter new delay in minutes:");
      int newDelayMinutes = Scan.scanInteger("Delay");

      register.updateDelay(trainDeparture, newDelayMinutes);
    } else {
      System.out.println("Train departure not found.");
    }
  }

  /**
   * Adds track to departure.
   */
  public void updateTrack() {
    System.out.println("Enter train number for which you want to update the track:");
    int trainNumber = Scan.scanInteger("Train number");
    TrainDeparture trainDeparture = register.searchDepartureTrainNum(trainNumber);

    if (trainDeparture != null) {
      System.out.println("Enter track");
      int newTrack = Scan.scanInteger("Track");
      register.updateTrack(trainDeparture, newTrack);
    } else {
      System.out.println("Train departure not found.");
    }
  }

  /**
   * Removes departure from list with train number taken from user.
   */
  public void removeTrainByTrainNumber() {
    System.out.println("Enter train number you want to delete:");
    int trainNumber = Scan.scanInteger("Train number");
    register.removeTrain(register.searchDepartureTrainNum(trainNumber));
  }

  /**
   * User gets to update the clock.
   */
  public void updateClock() {
    System.out.println("Enter new time of day (HH:mm): ");
    try {
      LocalTime newTime = Scan.scanDepartureTime();
      clock.updateClock(newTime);
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Prints header for table, with info about station and what time it currently is.
   * Bold format for top of header is from ChatGPT.
   */
  public void printTableHeader() {
    System.out.println("\u001B[1m Departures from Oslo Central Station:     "
            + "\u001B[0m" + clock.getCurrentTime());
    System.out.printf("%-11s %-6s %-7s %-12s %-7s %-5s%n",
            "Departure",
            "Line",
            "Number",
            "Destination",
            "Delay",
            "Track");
  }
}