import java.time.Duration;
import java.time.LocalTime;

/**
 * The {@code TrainDeparture} class represents a train departure from one station.
 * Contains a constructor to create train departures.
 * The class includes getters for all attributes, and setters for those that are logical to mutate
 * The constructor verifies the input of the parameters entered,
 * and throws exceptions on illegal data.
 *
 * @author 10047
 * @version 0.1
 */

public class TrainDeparture {
  private final int trainNumber;
  private final String line;
  private final LocalTime departureTime;
  private final String destination;
  private int track;
  private Duration delay;


  /**
   * Constructs a train departure by assigning the parameters below as attributes of the Item.
   * {@link #verifyStringParameter(String, String)},
   * {@link #verifyPositiveNumber(Number)}
   * {@link #validateNonNullDepartureTime(LocalTime)}
   * {@link #validateTrack(Number)}
   *
   * @param trainNumber a unique integer for a train departure.
   * @param line string, with a combination of numbers and letters.
   * @param departureTime LocalTime value for when the train leaves station
   * @param destination String value for destination of train
   * @param track int value for what track the train leaves from.
   */
  public TrainDeparture(int trainNumber, String line, LocalTime departureTime,
                        String destination, int track) {
    verifyPositiveNumber(trainNumber);
    verifyStringParameter(line, "Line");
    validateNonNullDepartureTime(departureTime);
    verifyStringParameter(destination, "Destination");
    validateTrack(track);

    this.trainNumber = trainNumber;
    this.line = line;
    this.departureTime = departureTime;
    this.destination = destination;
    this.track = track;
    this.delay = Duration.ZERO;
  }

  /**
   * Constructor for train-departure without chosen track.
   * Track is then automatically set to -1
   *
   * @param trainNumber a unique integer for a train departure.
   * @param line string, with a combination of numbers and letters.
   * @param departureTime LocalTime value for when the train leaves station
   * @param destination String value for destination of train
   */
  public TrainDeparture(int trainNumber, String line, LocalTime departureTime, String destination) {
    this (trainNumber, line, departureTime, destination, -1);
  }

  /**
   * Set the track to a positive number.
   *
   * @param track the track user assigns to a departure.
   */
  public void setTrack(int track) {
    try {
      validateTrack(track);
      this.track = track;
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Track can't be negative.");
    }
  }

  /**
   * Add a delay to the train.
   *
   * @param delay the delay the user wants to add, in minutes.
   * @throws IllegalArgumentException if the provided delay is negative.
   */
  public void setDelay(Duration delay) {
    if (delay.isNegative()) {
      throw new IllegalArgumentException("Delay has to be a non-negative duration.");
    }

    this.delay = this.delay.plus(delay);
  }

  /**
   * Returns the integer value representing train number.
   *
   * @return integer of the train number.
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Returns the representing value representing line of departure.
   *
   * @return String with a combination of integers and letters.
   */
  public String getLine() {
    return line;
  }

  /**
   * Returns the integer value representing the track.
   *
   * @return int Track train is departing from.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Returns the Duration value representing the delay.
   *
   * @return Duration of delay for departure.
   */
  public Duration getDelay() {
    return delay;
  }

  /**
   * Returns the departure time of the train.
   *
   * @return LocalTime representing the departure time.
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Returns String representing destination of the train.
   *
   * @return String with train destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Returns the departure time of the train with added delay.
   *
   * @return LocalTime representing the departure time with added delay.
   */
  public LocalTime getDepartureWithDelay() {
    return departureTime.plus(delay);
  }

  /**
   * Verifies a given Number by checking whether it is a positive number. The check fails if the
   * Number parameter is less than zero.
   *
   * @param parameter is the Number that is verified.
   *
   * @throws IllegalArgumentException if the Number parameter is a negative number. This is
   *                                  considered illegal because the train number cannot be less
   *                                  than zero.
   */
  private void verifyPositiveNumber(Number parameter)
          throws IllegalArgumentException {
    if (parameter.intValue() < 0) {
      throw new IllegalArgumentException("The value for the parameter "
              + "'train number' was a negative number, please try again.");
    }
  }

  /**
   * Verifies String is not empty. The check fails if the String parameter is blank.
   *
   * @param parameter is the String that is verified.
   * @param parameterName is the name of the String being checked. This is utilized throw a
   *                      specifying exception-message if the check fails.
   *
   * @throws IllegalArgumentException if the String parameter is either "" or only whitespaces.
   *                                  Then the String is considered illegal because it will contain
   *                                  insufficient information.
   */
  private void verifyStringParameter(String parameter, String parameterName)
          throws IllegalArgumentException {
    if (parameter.isBlank()) {
      throw new IllegalArgumentException("The string for the parameter '"
              + parameterName + "' was blank, please try again.");
    }
  }

  /**
   * Verifies a given LocalTime by checking whether it is not null.
   *
   * @param parameter is the LocalTime that is verified.
   *
   * @throws IllegalArgumentException if the LocalTime parameter is null. This is
   *                                  considered illegal because the departure time has to
   *                                  be specified.
   */
  private void validateNonNullDepartureTime(LocalTime parameter)
          throws IllegalArgumentException {
    if (parameter == null) {
      throw new IllegalArgumentException("The value for the parameter "
              + "'departureTime' was null, please provide a valid departure time.");
    }
  }

  /**
   * Verifies a given Number by checking whether it is a positive number or -1. The check fails if
   * the Number parameter is less than zero, except for -1.
   *
   * @param parameter is the Number that is verified.
   *
   * @throws IllegalArgumentException if the Number parameter is zero or less, but not -1. This is
   *                                  considered illegal because the track can not be negative,
   *                                  unless its assigned -1 which indicates it has not yet
   *                                  been assigned a proper track.
   */
  private void validateTrack(Number parameter)
          throws IllegalArgumentException {
    if (parameter.intValue() < 0 && parameter.intValue() != -1) {
      throw new IllegalArgumentException("The value for the parameter "
              + "'track' was invalid, please try again.");
    }
  }

  /**
   * Returns a formatted string representation of the TrainDeparture object.
   * The format includes departure time, line, train number, destination,
   * departure time with delay (if any), and track (if assigned).
   *
   * @return String representation of the TrainDeparture.
   */
  @Override
  public String toString() {
    return String.format("%-11s %-6s %-7s %-12s %-7s %-5s",
            departureTime,
            line,
            trainNumber,
            destination,
            delay.isZero() ? "" : getDepartureWithDelay(),
            track > 0 ? track : "");
  }
}
