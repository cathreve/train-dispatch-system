import java.time.LocalTime;

/**
 * The Clock class represents the clock used in a train departure management system.
 * It keeps track of the current time and provides a method to update the time.
 *
 * @author 10047
 * @version 0.1
 */
public class Clock {

  private LocalTime currentTime;

  /**
   * Initializes the clock with the minimum local time.
   */
  public Clock() {
    this.currentTime = LocalTime.MIN;
  }

  /**
   * Updates the clock with a new chosen time and ensuring it is valid.
   *
   * @param newTime The new time to set for the clock.
   */
  public void updateClock(LocalTime newTime) {
    if (newTime.isAfter(currentTime)) {
      currentTime = newTime;
    } else {
      throw new IllegalArgumentException("Invalid time. Please enter a time later than "
              + currentTime);
    }
  }

  /**
  * Gets the current time of the day.
  *
  * @return The current time of the day.
  */
  public LocalTime getCurrentTime() {
    return currentTime;
  }
}
